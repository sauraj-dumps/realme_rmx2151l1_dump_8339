## full_oppo6785-user 10 QP1A.190711.020 f98a4913a52475a1 release-keys
- Manufacturer: realme
- Platform: mt6785
- Codename: RMX2151L1
- Brand: realme
- Flavor: full_oppo6785-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: 0870_202103021932
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: RMX2151PU_11.A.87_0870_202103021932
- Branch: RMX2151PU_11.A.87_0870_202103021932
- Repo: realme_rmx2151l1_dump_8339


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
